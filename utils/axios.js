import axios from 'axios'
import cogoToast from 'cogo-toast';
import URLSearchParams from '@ungap/url-search-params'

axios.defaults.baseURL = `https://biz.youxiangshe.com`;
//axios.defaults.headers.post['Content-Type'] = "application/x-www-form-urlencoded";
axios.defaults.withCredentials = true
axios.interceptors.request.use(function (config) {
  config.validateStatus = function (status) {
    return status ; // default
  }
  return config;
})
axios.interceptors.response.use(function (response) {
    
    if (response.status === 200) {
      return Promise.resolve(response.data)
    } else {
      cogoToast.error(response.data.Message)
      return Promise.reject(response)
    }
}, function (error) {
  console.log(error)
}
)

export default axios