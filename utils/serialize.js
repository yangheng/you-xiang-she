export function serialize (data) {
  const temp = new URLSearchParams()
  for (let k in data) {
    temp.append(k, data[k])
  }
  return temp
}
export function showCode(e) {
    const modal = document.getElementsByClassName('modal')[0];
    let display = modal.style.display ;
    if (display == 'block') {
      modal.style.display = 'none'
    } else {
      modal.style.display = 'block'
    }
  }