module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "4Fgn":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return codeUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return contentTypeUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return articleListUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return articelUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return downloadInfoUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return loginUri; });
/* unused harmony export findUri */
/* unused harmony export resetUri */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return registerUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return feedbackUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return feedbackTypeUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return postTypeUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return postUri; });
// 验证码
const codeUri = '/service/MAF.APPs.Sys/UserLoginService/GetCheckCodeImageurl'; // 内容分类

const contentTypeUri = '/service/MAF.APPs.CMS/ContentService/GetPublicType'; // 内容文章列表

const articleListUri = '/service/MAF.APPs.CMS/ContentService/GetContentList'; // 内容

const articelUri = '/service/MAF.APPs.CMS/ContentService/GetContentById'; // 下载文章收集信息

const downloadInfoUri = '/service/MAF.APPs.CMS/ContentService/Download'; // 登录

const loginUri = '/service/MAF.APPs.YXS/ProviderService/Login'; // 找回密码

const findUri = '/service/MAF.APPs.Sys/UserLoginService/GetPassword'; // 重置密码

const resetUri = '/service/MAF.APPs.Sys/UserLoginService/ResetPassword'; // 注册

const registerUri = '/service/MAF.APPs.YXS/ProviderService/Apply'; // 反馈

const feedbackUri = '/service/MAF.APPs.Sys/FeedbackService/CreateFeedback'; // 反馈类型

const feedbackTypeUri = '/service/MAF.APPs.Sys/FeedbackService/GetTypeList'; // 公告列表

const postTypeUri = '/service/MAF.APPs.CMS/ContentService/GetPrivateType'; // 公告详情

const postUri = '/service/MAF.APPs.CMS/ContentService/GetContentById';

/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "4vsW":
/***/ (function(module, exports) {

module.exports = require("node-fetch");

/***/ }),

/***/ "5jvD":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"nav": "provider_nav__q56nB",
	"content": "provider_content__pv-4k",
	"register": "provider_register__1Azll",
	"singale": "provider_singale__3t0yu",
	"cardWrapper": "provider_cardWrapper__1wVkI",
	"articleList": "provider_articleList__2Cbaz",
	"card": "provider_card__2q4Zg",
	"title": "provider_title__38s-u",
	"card1": "provider_card1__3EkWm",
	"card2": "provider_card2__u6yOH",
	"card3": "provider_card3__Bba6S",
	"top": "provider_top__2DBxY",
	"bottom": "provider_bottom__31oiq",
	"card4": "provider_card4__15v_m"
};


/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("yoiM");


/***/ }),

/***/ "9FVX":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getArticleList; });
/* unused harmony export getPostType */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getArticle; });
/* harmony import */ var node_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("4vsW");
/* harmony import */ var node_fetch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(node_fetch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _const__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("4Fgn");



const server = uri => `http://${process.env.HOST}:${process.env.PORT}${uri}`;

const axios = {
  post(uri, data) {
    return node_fetch__WEBPACK_IMPORTED_MODULE_0___default()(uri);
  },

  get(uri, data) {
    let params = [];

    for (let key in data) {
      params.push(`${key}=${data[key]}`);
    }

    return node_fetch__WEBPACK_IMPORTED_MODULE_0___default()(`${uri}?${params.join('&')}`);
  }

};
function getArticleList(params) {
  return axios.get(server(_const__WEBPACK_IMPORTED_MODULE_1__[/* articleListUri */ "b"]), params);
}
function getPostType(params) {
  return axios.get(server(_const__WEBPACK_IMPORTED_MODULE_1__[/* postTypeUri */ "i"]), params);
}
function getTypes(params) {
  return axios.get(server(_const__WEBPACK_IMPORTED_MODULE_1__[/* contentTypeUri */ "d"]), params);
}
function getArticle(params) {
  return axios.get(server(_const__WEBPACK_IMPORTED_MODULE_1__[/* articelUri */ "a"]), params);
}

/***/ }),

/***/ "9HB/":
/***/ (function(module, exports) {

module.exports = require("cogo-toast");

/***/ }),

/***/ "9b7L":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

/* harmony default export */ __webpack_exports__["a"] = (({
  html
}) => {
  const ref = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useLayoutEffect"])(() => {
    const doc = ref.current.contentWindow.document;
    doc.head.innerHTML = '<base target="_top"></base>';
    const _body = doc.body;
    _body.innerHTML = html;
    ref.current.height = _body.scrollHeight;
  });
  return __jsx("iframe", {
    ref: ref
  });
});

/***/ }),

/***/ "Jpla":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
/* harmony default export */ __webpack_exports__["a"] = (function () {
  return __jsx("div", {
    className: "footer"
  }, __jsx("div", {
    className: "container"
  }, __jsx("div", null, __jsx("a", {
    href: "/register"
  }, "\u4F9B\u5E94\u5546\u5165\u9A7B"), __jsx("a", {
    href: "/partner"
  }, "\u5546\u52A1\u5408\u4F5C"), __jsx("a", {
    href: "/feedback"
  }, "\u610F\u89C1\u53CD\u9988"), __jsx("p", null, "\u4F18\u4EAB\u793E\u2014\u2014", __jsx("strong", null, "\u5206\u4EAB\u3001\u5206\u9500"), "\u65B0\u578B", __jsx("strong", null, "\u793E\u4EA4\u7535\u5546\u5E73\u53F0")), __jsx("p", {
    className: "slogan"
  }, "          \u2014\u2014\u5C06\u4F18\u4EAB\u751F\u6D3B\u5E26\u7ED9\u8EAB\u8FB9\u6BCF\u4E00\u4E2A\u4EBA")), __jsx("div", {
    className: "copyright"
  }, __jsx("p", null, "Copyright \xA9 2020 youxiangshe.com ", __jsx("br", null), "\u4EACICP\u590710216015\u53F7-5")), __jsx("div", {
    className: "codeList"
  }, __jsx("div", null, __jsx("img", {
    src: "/code.png",
    alt: "\u4F18\u4EAB\u5BA2\u5C0F\u7A0B\u5E8F"
  }), __jsx("p", null, "\u4F18\u4EAB\u5BA2\u5C0F\u7A0B\u5E8F")), __jsx("div", null, __jsx("img", {
    src: "/wechat.png",
    alt: "\u4F18\u4EAB\u5B66\u5458\u8BA2\u9605\u53F7"
  }), __jsx("p", null, "\u4F18\u4EAB\u5B66\u5458\u8BA2\u9605\u53F7")))), __jsx("div", null), __jsx("div", null));
});

/***/ }),

/***/ "K2gz":
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),

/***/ "OnoF":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"articleList": "knoweldge_articleList__1D8WO",
	"left": "knoweldge_left__1Psfv",
	"right": "knoweldge_right__AKk1Q",
	"extra": "knoweldge_extra__1H8zR",
	"article": "knoweldge_article__8nykz",
	"body": "knoweldge_body__cuLlq",
	"post": "knoweldge_post__3OHNj",
	"links": "knoweldge_links__1NQiw",
	"after": "knoweldge_after__1-84W",
	"nav": "knoweldge_nav__3ysmx",
	"download": "knoweldge_download__2AIPQ",
	"info": "knoweldge_info__2LqEm",
	"tips": "knoweldge_tips__BdoWY",
	"hotList": "knoweldge_hotList__mqb_6",
	"subnav": "knoweldge_subnav__3P42R",
	"active": "knoweldge_active__2iP3j"
};


/***/ }),

/***/ "Yavo":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Nav; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("xV2i");
/* harmony import */ var _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("4Q3z");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _utils_serialize__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("jJwv");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





function Nav(params) {
  const {
    0: active,
    1: setActive
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])('');
  const ref = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  function activeTag(path) {
    if (router.pathname === path) return _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.active;
    if (router.pathname.indexOf(path) != -1 && path !== '/') return _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.active;
    return null;
  }

  return __jsx("header", {
    className: _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.navbar
  }, __jsx("div", {
    className: "modal",
    ref: ref
  }, __jsx("img", {
    src: "/code.png"
  })), __jsx("div", {
    className: classnames__WEBPACK_IMPORTED_MODULE_2___default()(_scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.container, 'clearfix')
  }, __jsx("a", {
    href: "/",
    className: _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.logo
  }, __jsx("img", {
    src: "/logo.png",
    alt: "\u4F18\u4EAB\u793E\uFF08youxiangshe.com\uFF09-\u4F18\u8D28\u5546\u54C1\u793E\u4EA4\u7535\u5546\u670D\u52A1\u5E73\u53F0"
  })), __jsx("div", {
    className: [_scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.nav]
  }, __jsx("a", {
    href: "/",
    className: activeTag('/')
  }, "\u9996\u9875"), __jsx("a", {
    href: "/provider",
    className: activeTag('/provider')
  }, "\u4F9B\u5E94\u5546"), __jsx("a", {
    href: "/k/list/down",
    className: activeTag('/k')
  }, "\u6DA8\u77E5\u8BC6"), __jsx("a", {
    href: "/about",
    className: activeTag('/about')
  }, "\u5173\u4E8E\u6211\u4EEC")), __jsx("div", {
    className: _scss_navbar_module_scss__WEBPACK_IMPORTED_MODULE_1___default.a.related
  }, __jsx("a", {
    href: "/login",
    target: "_blank"
  }, "\u4F9B\u5E94\u5546\u540E\u53F0"))));
}

/***/ }),

/***/ "bMwp":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, "g", function() { return /* binding */ register; });
__webpack_require__.d(__webpack_exports__, "a", function() { return /* binding */ feedback; });
__webpack_require__.d(__webpack_exports__, "e", function() { return /* binding */ getFeedbackType; });
__webpack_require__.d(__webpack_exports__, "c", function() { return /* binding */ getCodeUri; });
__webpack_require__.d(__webpack_exports__, "f", function() { return /* binding */ login; });
__webpack_require__.d(__webpack_exports__, "d", function() { return /* binding */ getDownloadInfo; });
__webpack_require__.d(__webpack_exports__, "b", function() { return /* binding */ forget; });

// UNUSED EXPORTS: getContentType, getPost, getPostType, getArticleList, getArticle

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: external "cogo-toast"
var external_cogo_toast_ = __webpack_require__("9HB/");
var external_cogo_toast_default = /*#__PURE__*/__webpack_require__.n(external_cogo_toast_);

// EXTERNAL MODULE: external "@ungap/url-search-params"
var url_search_params_ = __webpack_require__("wHFt");

// CONCATENATED MODULE: ./utils/axios.js



external_axios_default.a.defaults.baseURL = `https://biz.youxiangshe.com`; //axios.defaults.headers.post['Content-Type'] = "application/x-www-form-urlencoded";

external_axios_default.a.defaults.withCredentials = true;
external_axios_default.a.interceptors.request.use(function (config) {
  config.validateStatus = function (status) {
    return status; // default
  };

  return config;
});
external_axios_default.a.interceptors.response.use(function (response) {
  if (response.status === 200) {
    return Promise.resolve(response.data);
  } else {
    external_cogo_toast_default.a.error(response.data.Message);
    return Promise.reject(response);
  }
}, function (error) {
  console.log(error);
});
/* harmony default export */ var axios = (external_axios_default.a);
// EXTERNAL MODULE: ./api/const.js
var api_const = __webpack_require__("4Fgn");

// CONCATENATED MODULE: ./api/index.js



function getContentType(params) {
  return axios.get(api_const["d" /* contentTypeUri */], params);
}
function getPost(params) {
  return axios.get(api_const["j" /* postUri */], params);
}
function getPostType(params) {
  return axios.get(api_const["i" /* postTypeUri */], params);
}
function getArticleList(params) {
  return axios.get(api_const["b" /* articleListUri */], {
    params
  });
}
function getArticle(params) {
  return axios.get(api_const["a" /* articelUri */], {
    params
  });
}
function register(params) {
  return axios.post(api_const["k" /* registerUri */], params);
}
function feedback(params) {
  return axios.post(api_const["g" /* feedbackUri */], params);
}
function getFeedbackType() {
  return axios.get(api_const["f" /* feedbackTypeUri */]);
}
function getCodeUri(params) {
  return axios.get(api_const["c" /* codeUri */], params);
}
function login(params) {
  return axios.post(api_const["h" /* loginUri */], params);
}
function getDownloadInfo(params) {
  let win = window.open(`${axios.defaults.baseURL}${api_const["e" /* downloadInfoUri */]}?${params}`);
  win.addEventListener('beforeunload', function () {
    external_cogo_toast_default.a.success('下载完成');
  });
}
function forget() {
  return new Promise((resolve, reject) => {});
}

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "jJwv":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return serialize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return showCode; });
function serialize(data) {
  const temp = new URLSearchParams();

  for (let k in data) {
    temp.append(k, data[k]);
  }

  return temp;
}
function showCode(e) {
  const modal = document.getElementsByClassName('modal')[0];
  let display = modal.style.display;

  if (display == 'block') {
    modal.style.display = 'none';
  } else {
    modal.style.display = 'block';
  }
}

/***/ }),

/***/ "wHFt":
/***/ (function(module, exports) {

module.exports = require("@ungap/url-search-params");

/***/ }),

/***/ "xV2i":
/***/ (function(module, exports) {

// Exports
module.exports = {
	"navbar": "navbar_navbar__1zcCu",
	"nav": "navbar_nav__1LFJq",
	"active": "navbar_active__TEuMX",
	"related": "navbar_related__3rA4d",
	"logo": "navbar_logo__2QPUx",
	"container": "navbar_container__20LCw"
};


/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "yoiM":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getServerSideProps", function() { return getServerSideProps; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("xnum");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("Jpla");
/* harmony import */ var _components_nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("Yavo");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("K2gz");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _scss_provider_module_scss__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("5jvD");
/* harmony import */ var _scss_provider_module_scss__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_scss_provider_module_scss__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("OnoF");
/* harmony import */ var _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _utils_serialize__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("jJwv");
/* harmony import */ var cogo_toast__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("9HB/");
/* harmony import */ var cogo_toast__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(cogo_toast__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _api_index__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("bMwp");
/* harmony import */ var _api_server__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("9FVX");
/* harmony import */ var _components_iframe__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("9b7L");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }













/* harmony default export */ __webpack_exports__["default"] = (function ({
  article,
  type,
  parent,
  hot,
  rec,
  id
}) {
  console.log(article);
  const {
    0: form,
    1: setform
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    ContactName: '',
    ContactEmail: '',
    ContactMobile: '',
    ContactCompanyName: '2'
  });

  function updateForm(e, key) {
    e.preventDefault();

    let data = _objectSpread({}, form);

    data[key] = e.target.value;
    setform(data);
  }

  function download() {
    if (!form.ContactName) return cogo_toast__WEBPACK_IMPORTED_MODULE_8___default.a.error('需要填写正确姓名');
    if (!form.ContactMobile || !/^1[3-9]\d{9}$/.test(form.ContactMobile)) return cogo_toast__WEBPACK_IMPORTED_MODULE_8___default.a.error('需要填写正确的联系电话');
    if (!form.ContactEmail || !/^[A-Za-z0-9]+([_\.][A-Za-z0-9]+)*@([A-Za-z0-9\-]+\.)+[A-Za-z]{2,6}$/.test(form.ContactEmail)) return cogo_toast__WEBPACK_IMPORTED_MODULE_8___default.a.error('需要填写正确的邮箱');

    const data = _objectSpread(_objectSpread({}, form), {}, {
      File_Id: article.FileDTOS[0].Id
    });

    let hash = Object(_utils_serialize__WEBPACK_IMPORTED_MODULE_7__[/* serialize */ "a"])(data).toString();
    Object(_api_index__WEBPACK_IMPORTED_MODULE_9__[/* getDownloadInfo */ "d"])(hash);
  }

  let showLink = article.BeforeId || article.AfterId;
  return __jsx("div", {
    className: "container"
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, null, __jsx("title", null, article.ContentDTO.Title, "-\u6587\u6863\u4E0B\u8F7D-\u6DA8\u77E5\u8BC6-\u4F18\u4EAB\u793E(youxiangshe.com)"), __jsx("meta", {
    name: "keywords",
    content: article.ContentDTO.Keywords
  }), __jsx("meta", {
    name: "description",
    content: article.ContentDTO.Description
  })), __jsx(_components_nav__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], null), __jsx("main", null, __jsx("div", {
    className: classnames__WEBPACK_IMPORTED_MODULE_4___default()(_scss_provider_module_scss__WEBPACK_IMPORTED_MODULE_5___default.a.nav, _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.nav)
  }, "\u5F53\u524D\u4F4D\u7F6E\uFF1A", __jsx("a", {
    href: "/"
  }, "\u9996\u9875"), ">", __jsx("a", {
    href: "/k/list"
  }, "\u6DA8\u77E5\u8BC6"), ">", __jsx("a", {
    href: `/k/list/${parent.Code}/1`
  }, parent.Name), ">", __jsx("strong", null, article.ContentDTO && article.ContentDTO.Title)), __jsx("div", {
    className: _scss_provider_module_scss__WEBPACK_IMPORTED_MODULE_5___default.a.content
  }, __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.article
  }, __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.body
  }, __jsx("h1", null, article.ContentDTO && article.ContentDTO.Title), __jsx("p", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.extra
  }, __jsx("span", null, __jsx("img", {
    src: "/time.png",
    alt: "\u65F6\u95F4"
  }), article.ContentDTO && article.ContentDTO.CreatedOn), __jsx("span", null, __jsx("img", {
    src: "/cc-eye.png",
    alt: "\u9605\u89C8\u6570"
  }), article.ContentDTO && article.ContentDTO.VisitTimes)), __jsx("article", {
    className: "article"
  }, __jsx("div", {
    className: "machine",
    dangerouslySetInnerHTML: {
      __html: article.ContentDTO && article.ContentDTO.Content
    }
  }), __jsx(_components_iframe__WEBPACK_IMPORTED_MODULE_11__[/* default */ "a"], {
    html: article.ContentDTO && article.ContentDTO.Content
  }))), article.FileDTOS.length ? __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.download
  }, __jsx("form", null, __jsx("div", null, __jsx("h3", null, "\u586B\u5199\u8D44\u6599\uFF0C\u514D\u8D39\u4E0B\u8F7D"), __jsx("p", null, "\u8BF7\u8BA4\u771F\u586B\u5199\u8D44\u6599\uFF0C\u4EE5\u4FBF\u4E8E\u6211\u4EEC\u4E3A\u4F60\u63A8\u8350\u66F4\u591A\u7CBE\u534E\u7684\u5185\u5BB9"), __jsx("div", {
    className: "formItem"
  }, __jsx("label", null, __jsx("em", null, "*"), "\u59D3\u540D\uFF1A"), __jsx("input", {
    value: form.ContactName,
    onInput: e => updateForm(e, 'ContactName')
  })), __jsx("div", {
    className: "formItem"
  }, __jsx("label", null, __jsx("em", null, "*"), "\u624B\u673A\uFF1A"), __jsx("input", {
    value: form.ContactMobile,
    onInput: e => updateForm(e, 'ContactMobile')
  })), __jsx("div", {
    className: "formItem"
  }, __jsx("label", null, __jsx("em", null, "*"), "\u90AE\u7BB1\uFF1A"), __jsx("input", {
    value: form.ContactEmail,
    onInput: e => updateForm(e, 'ContactEmail')
  })))), __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.info
  }, __jsx("h5", null, "\u201C\u5982\u679C\u559C\u6B22\u6B64\u6587\u7AE0\uFF0C\u70B9\u51FB\u4E0B\u65B9\u6309\u94AE\u8FDB\u884C\u4E0B\u8F7D\u54E6\u201C"), __jsx("p", null, "\u5DF2\u7ECF\u6709", article.ContentDTO && article.ContentDTO.DownTimes, " \u4EBA\u4E0B\u8F7D"), __jsx("p", null, __jsx("a", {
    className: "btn",
    onClick: download
  }, "\u7ACB\u5373\u4E0B\u8F7D\uFF08", article.FileDTOS.length && article.FileDTOS[0].Filesize, "\uFF09")), __jsx("p", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.tips
  }, "\u624B\u673A\u5FAE\u4FE1\u7528\u6237\u8BF7\u4F7F\u7528\u53F3\u4E0A\u89D2\u201C\u5728\u6D4F\u89C8\u5668\u4E2D\u6253\u5F00\u201C\u518D\u4E0B\u8F7D"))) : null, showLink ? __jsx("div", {
    className: classnames__WEBPACK_IMPORTED_MODULE_4___default()(_scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.links, 'clearfix')
  }, __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.pre
  }, "\u4E0A\u4E00\u7BC7\uFF1A", __jsx("a", {
    href: `/k/${type}/${article.BeforeId}`
  }, article.BeforeTitle)), __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.after
  }, "\u4E0B\u4E00\u7BC7\uFF1A", __jsx("a", {
    href: `/k/${type}/${article.AfterId}`
  }, article.AfterTitle))) : null), __jsx("div", {
    className: _scss_knoweldge_module_scss__WEBPACK_IMPORTED_MODULE_6___default.a.hotList
  }, __jsx("section", null, __jsx("h2", null, __jsx("img", {
    src: "/icon.png",
    alt: "\u76F8\u5173\u9605\u8BFB"
  }), "\u76F8\u5173\u9605\u8BFB"), __jsx("div", null, rec.map((item, index) => __jsx("a", {
    key: index,
    href: `/k/${type}/${item.ID}`
  }, item.Title)))), __jsx("section", null, __jsx("h2", null, __jsx("img", {
    src: "/hot.png",
    alt: "\u70ED\u95E8\u7535\u5546\u6587\u7AE0\u6392\u884C"
  }), "\u70ED\u95E8\u7535\u5546\u6587\u7AE0\u6392\u884C"), __jsx("div", null, hot.map((item, index) => __jsx("a", {
    key: index,
    href: `/k/${type}/${item.ID}`
  }, item.Title))))))), __jsx(_components_footer__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], null));
});
async function getServerSideProps({
  query
}) {
  const {
    id,
    type
  } = query;
  const data = {
    typeCode: type,
    pageNo: 1,
    pageSize: 10,
    order: 'CreatedOn',
    isAsc: false,
    isCommend: 0
  };
  let res = await Object(_api_server__WEBPACK_IMPORTED_MODULE_10__[/* getTypes */ "c"])();
  const types = await res.json();
  let parent = types.find(item => item.Code == type);
  res = await Object(_api_server__WEBPACK_IMPORTED_MODULE_10__[/* getArticle */ "a"])({
    id
  });
  const article = await res.json();
  const hotReq = await Object(_api_server__WEBPACK_IMPORTED_MODULE_10__[/* getArticleList */ "b"])(_objectSpread(_objectSpread({}, data), {}, {
    excludeId: id,
    order: 'VisitTimes'
  }));
  const recReq = await Object(_api_server__WEBPACK_IMPORTED_MODULE_10__[/* getArticleList */ "b"])(_objectSpread(_objectSpread({}, data), {}, {
    excludeId: id,
    isCommend: 1
  }));
  const contentReq = await Object(_api_server__WEBPACK_IMPORTED_MODULE_10__[/* getArticleList */ "b"])(data);
  const hot = await hotReq.json();
  const rec = await recReq.json();
  return {
    props: {
      article,
      type,
      parent,
      hot: hot.models,
      rec: rec.models,
      id
    }
  };
}

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });