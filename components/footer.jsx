
export default function () {
  return (
  <div className="footer">
        <div className="container">
          <div>
            <a href="/register">
              供应商入驻
            </a>
            <a href="/partner">
              商务合作
            </a>
            <a href="/feedback">
              意见反馈
            </a>
            {
              /* 
              <a href="/posts/list">
              站内公告
            </a>
               */
            }
            
            <p>优享社——<strong>分享、分销</strong>新型<strong>社交电商平台</strong></p>
            <p className="slogan">          ——将优享生活带给身边每一个人</p>
          </div>
          <div className="copyright">
            <p>
            Copyright © 2020 youxiangshe.com <br/>
            京ICP备10216015号-5
            </p>
          </div>
          <div className="codeList">
          
            
            <div>
              <img src="/code.png" alt="优享客小程序" />
              <p>优享客小程序</p>
            </div>
            
            <div>
              <img src="/wechat.png" alt="优享学员订阅号" />
              <p>优享学员订阅号</p>
            </div>
           
          
            
          </div>
        </div>
        <div>

        </div>
        <div>

        </div>
      </div>
  )
}