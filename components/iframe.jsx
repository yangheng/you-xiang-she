import { 
  useRef,
  useLayoutEffect
} from 'react'
export default ({ html}) => {
  const ref = useRef(null)
  useLayoutEffect(() => {
    const doc = ref.current.contentWindow.document;
    doc.head.innerHTML = '<base target="_top"></base>'
    const _body = doc.body;
    _body.innerHTML = html;
    ref.current.height = _body.scrollHeight;
  })
  return (
    <iframe ref={ref}>
    </iframe>
  )
}