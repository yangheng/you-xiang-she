import style from '@/scss/navbar.module.scss'
import cs from 'classnames'
import { useRouter } from 'next/router'
import { useState, useRef } from 'react'
import { showCode } from '@/utils/serialize'

export default function Nav(params) {
  const [active, setActive] = useState('')
  const ref = useRef(null)
  const router = useRouter()
  
  
  function activeTag(path) {
    if (router.pathname === path) return style.active;
    if ((router.pathname.indexOf(path)!= -1) && (path !== '/')) return style.active;
    return null
  }

  return (
    <header className={style.navbar}>
      <div className="modal" ref={ref}>
      <img src="/code.png"/>
    </div>
      <div className={cs(style.container, 'clearfix')}>
        <a href="/" className={style.logo}>
           <img src="/logo.png" alt="优享社（youxiangshe.com）-优质商品社交电商服务平台"/>
        </a>
        <div className={[style.nav]} >
          <a href="/" className={activeTag('/')}>
            首页
          </a>
          <a href="/provider" className={activeTag('/provider')}>
            供应商
          </a>
          
          <a href="/k/list/down" className={activeTag('/k')}>
            涨知识
          </a>
          
         
          <a href="/about" className={activeTag('/about')}>
              关于我们
          </a>
          
         
        </div>
        <div className={style.related}>
          <a href="/login" target="_blank">
            供应商后台
          </a>
          {
            /* 
            <a onClick={showCode}>
            优享小程序
          </a>
            */
          }
          
        </div>
      </div>
    </header>
  )
}