import axios from '@/utils/axios'
import cogoToast from 'cogo-toast';
import {
  codeUri,
  contentTypeUri,
  articleListUri,
  articelUri,
  downloadInfoUri,
  loginUri,
  findUri,
  resetUri,
  registerUri,
  feedbackUri,
  feedbackTypeUri,
  postTypeUri,
  postUri
} from './const'

export function getContentType(params) {
  return axios.get(contentTypeUri, params)
}

export function getPost(params) {
  return axios.get(postUri, params)
}
export function getPostType(params) {
  return axios.get(postTypeUri, params)
}


export function getArticleList(params) {
  return axios.get(articleListUri, {params})
}

export function getArticle(params) {
  return axios.get(articelUri, {params})
}

export function register(params) {
  return axios.post(registerUri, params)
}
export function feedback(params) {
  return axios.post(feedbackUri, params)
}
export function getFeedbackType() {
  return axios.get(feedbackTypeUri)
}
export function getCodeUri(params) {
  return axios.get(codeUri, params)
}
export function login(params) {
  return axios.post(loginUri, params)
}
export function getDownloadInfo(params) {
  let win = window.open(`${axios.defaults.baseURL}${downloadInfoUri}?${params}`)
  win.addEventListener('beforeunload', function() {
    cogoToast.success('下载完成')
  })
}

export function forget() {
  return new Promise((resolve, reject) => {
    
  })
}