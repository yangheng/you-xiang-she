// 验证码
export const codeUri = '/service/MAF.APPs.Sys/UserLoginService/GetCheckCodeImageurl'

// 内容分类
export const contentTypeUri = '/service/MAF.APPs.CMS/ContentService/GetPublicType'

// 内容文章列表
export const articleListUri = '/service/MAF.APPs.CMS/ContentService/GetContentList'

// 内容
export const articelUri = '/service/MAF.APPs.CMS/ContentService/GetContentById'

// 下载文章收集信息
export const downloadInfoUri = '/service/MAF.APPs.CMS/ContentService/Download'

// 登录
export const loginUri = '/service/MAF.APPs.YXS/ProviderService/Login'

// 找回密码
export const findUri = '/service/MAF.APPs.Sys/UserLoginService/GetPassword'

// 重置密码
export const resetUri = '/service/MAF.APPs.Sys/UserLoginService/ResetPassword'

// 注册
export const registerUri = '/service/MAF.APPs.YXS/ProviderService/Apply'

// 反馈
export const feedbackUri = '/service/MAF.APPs.Sys/FeedbackService/CreateFeedback'

// 反馈类型
export const feedbackTypeUri = '/service/MAF.APPs.Sys/FeedbackService/GetTypeList'

// 公告列表
export const postTypeUri = '/service/MAF.APPs.CMS/ContentService/GetPrivateType'

// 公告详情
export const postUri = '/service/MAF.APPs.CMS/ContentService/GetContentById'