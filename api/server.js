import fetch from 'node-fetch'
import {
  contentTypeUri,
  articelUri,
  articleListUri,
  postTypeUri
} from './const'
const server = (uri) => `http://${process.env.HOST}:${process.env.PORT}${uri}`

const axios = {
  post(uri, data) {
    return fetch(uri)
  },
  get(uri, data) {
    let params = []
    for (let key in data) {
      params.push(`${key}=${data[key]}`)
    }
    return fetch(`${uri}?${params.join('&')}`)
  }
}
export function getArticleList(params) {
  return axios.get(server(articleListUri), params)
}
export function getPostType(params) {
  return axios.get(server(postTypeUri), params)
}
export function getTypes(params) {
  return axios.get(server(contentTypeUri), params)
}

export function getArticle(params) {
  return axios.get(server(articelUri), params)
}
