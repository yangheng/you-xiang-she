import Head from 'next/head'
import Nav from '@/components/nav'
import cogoToast from 'cogo-toast';
import { useState } from 'react'
import { getCodeUri, forget } from '@/api/index'
import Footer from '@/components/footer'
import style from '@/scss/login.module.scss'
import { serialize } from '@/utils/serialize'

export default function Home() {
  const [form, setForm] = useState({
    email: '',
    code: ''
  })
  const [codeUri, setUri] = useState(getCodeUri())

  function updateUri() {
    setUri(`${getCodeUri()}?num=${Math.random()}`)
  }
  function updateForm(e, key) {
    e.preventDefault()
    let data = {...form}
    data[key] = e.target.value
    setForm(data)
  }
  function resetFn() {
    if (!form.email || 
        !/^[A-Za-z0-9]+([_\.][A-Za-z0-9]+)*@([A-Za-z0-9\-]+\.)+[A-Za-z]{2,6}$/.test(form.email)
       ) return cogoToast.error('需要填写正确的邮箱');
    if (!form.code) return cogoToast.error('需要填写验证码');

    forget(serialize(form)).then(data => {
      //todo: 跳转到后台管理端
    }).catch(err => {
      cogoToast.error('密码找回失败了')
    })
  }

  return (
    <div className="container">
      <Head>
        <title>供应商找回密码-供应商-优享社(youxiangshe.com)</title>
        <meta name="keywords" content="供应商找回密码"/>
        <meta name="description" content="供应商找回密码页面，可以通过签约邮件重置密码。"/>
      </Head>
      <Nav/>
      <main className="default">
        <form>
          <h1>供应商找回密码</h1>
          <p>请认真填写，您的反馈是我们前进的动力</p>
          <div>
            <div className="formItem">
              <label>邮件地址：</label>
              <input value={form.email} onInput={e=>updateForm(e, 'email')}></input>
            </div>
           <div className="formItem">
              <label>验证码：</label>
              <input className="code" value={form.code} onInput={e=>updateForm(e, 'code')}></input>
              <img src={codeUri}/>
              <i onClick={updateUri}>刷新</i>
            </div>
            <div className="formItem">
              <label> </label>
              <a className="btn" onClick={resetFn}>重置密码</a>
            </div>
          </div>
          <div>
          <p className={style.formTips}>
            <p>
            只支持供应商，优享客请访问微信小程序
          </p>
          </p>
          
        </div>
        </form>
        
      </main>
      <Footer/>
      
    </div>
  )
}
