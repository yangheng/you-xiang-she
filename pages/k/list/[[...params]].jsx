import Head from 'next/head'
import cx from 'classnames'
import Footer from '@/components/footer'
import Nav from '@/components/nav'
import style from '@/scss/provider.module.scss'
import style1 from '@/scss/knoweldge.module.scss'
//import { getArticleList } from '@/api/index'
import Pagination from 'rc-pagination';
import { getTypes, getArticleList } from '@/api/server'
function Knoweldge ({ types, hot, rec, content, page, code}) {

  function changePage (current) {
    window.location.href = `/k/list/${code}/${current}`
  }
  const currentType = types.find(item => item.Code === code )
  return (
    <div className="container">
      <Head>
        <title>{`${currentType.Name}(第${page.current}页)-涨知识-优享社(youxiangshe.com)`}</title>
        <meta name="keywords" content="社交电商内容,电商文档,销售技巧"/>
        <meta name="description" content="涨知识（优享学院）是优享社整理提供的社交电商内容聚合栏目，涵盖了相关电商文档、在线教程、销售技巧、营销案例等与社交电商相关的知识，帮助优享客提升盈利能力。"/>
      </Head>
      <Nav/>
      <main>
        
        <div className={style.nav}>
         {
          types.map((item, index) => {
            let node = null;
            if (code === item.Code) {
              node = (
                <h1 className={cx(style1.subnav, style1.active)} key={index}>
                  <a href={`/k/list/${item.Code}`}>{item.Name}</a>
                </h1>)
            } else {
              node = (
                <div key={index} className={style1.subnav}>
                  <a href={`/k/list/${item.Code}`}>{item.Name}</a>
                </div>
              )
            }
            return node;
          })
         }
        </div>

        <div className={style.content}>
          <div className={style1.articleList}>
            {
              content.map((item, index) => {
                return (
                  <section key={`article${index}`}>
                    <div className={style1.left}>
                      <img src={item.PicUrl} alt={item.Title}/>
                    </div>
                    <div className={style1.right}>
                      <h4><a href={`/k/${code}/${item.ID}`}>{item.Title}</a></h4>
                      <article>{item.Description}</article>
                      <p className={style1.extra}>
                        <span><img src="/time.png"/> {item.CreatedOn}</span>
                        
                      </p>
                    </div>
                  </section>
                )
              })
            }
            <div className="pagination">
              <Pagination current={page.current} total={page.total} pageSize={10} activeClassName="active" onChange={changePage}/>
            </div>
          </div>
          <div className={style1.hotList}>
            <section>
              <h2><img src="/icon.png" alt="相关阅读"/>相关阅读</h2>
              <div>
                {
                  rec.map((item, index) => <a key={index} href={`/k/${code}/${item.ID}`}>{item.Title}</a>)
                }
              </div>
            </section>
            
            <section>
              <h2> <img src="/hot.png" alt="热门电商文章排行"/>热门电商文章排行</h2>
              <div>
                {
                  hot.map((item, index) => <a key={index} href={`/k/${code}/${item.ID}`}>{item.Title}</a>)
                }
              </div>
            </section>
          </div>
        </div>
      </main>
    <Footer/>
    </div>
  )
}

export async function getServerSideProps({ query }) {
  const data = { typeCode: '', excludeId: '', pageNo: 1, pageSize: 10, order: 'CreatedOn', isAsc: false, isCommend: 0}

  const { params } = query;
  let res = null;
  let code = null;
  let typeId = null;
  res = await getTypes();
  const types = await res.json();
  if (!params) {
    code = types[0].Code;
    //typeId = types[0].Id;
  } else {
    code = params[0];
    data.pageNo = params[1] || 1;
    //typeId = types.find(item => item.Code == code).Id;
  }

  data.typeCode = code
  const hotReq = await getArticleList({...data, order: 'VisitTimes' })
  const recReq = await getArticleList({...data, isCommend: 1 })
  const contentReq = await getArticleList(data)
  const hot = await hotReq.json()
  const rec = await recReq.json()
  const content = await contentReq.json()
  return {
    props: { 
      code: code,
      types,
      content: content.models,
      hot: hot.models,
      rec: rec.models,
      page: {
        total: content.total,
        current: data.pageNo
      }
    }
  }
}

export default Knoweldge