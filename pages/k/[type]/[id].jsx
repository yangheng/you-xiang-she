import Head from 'next/head'
import { useState, useEffect } from 'react'
import Footer from '@/components/footer'
import Nav from '@/components/nav'
import cx from 'classnames'
import st from '@/scss/provider.module.scss'
import style from '@/scss/knoweldge.module.scss'
import { serialize } from '@/utils/serialize'
import cogoToast from 'cogo-toast';
import { getDownloadInfo } from '@/api/index'
import { getTypes, getArticle, getArticleList } from '@/api/server'
import Iframe from '@/components/iframe'

export default function({ article, type, parent, hot, rec, id }) {
  console.log(article)
  const [form, setform] = useState({
    ContactName: '',
    ContactEmail: '',
    ContactMobile: '',
    ContactCompanyName: '2'
  })
  
  function updateForm(e, key) {
    e.preventDefault()
    let data = {...form}
    data[key]= e.target.value
    setform(data)
  }
  
  
  
  
  function download() {
    if (!form.ContactName) return cogoToast.error('需要填写正确姓名');
    if (!form.ContactMobile || !/^1[3-9]\d{9}$/.test(form.ContactMobile)) return cogoToast.error('需要填写正确的联系电话');
    if (!form.ContactEmail || 
        !/^[A-Za-z0-9]+([_\.][A-Za-z0-9]+)*@([A-Za-z0-9\-]+\.)+[A-Za-z]{2,6}$/.test(form.ContactEmail)
      ) return cogoToast.error('需要填写正确的邮箱');

    const data = {
      ...form, File_Id: article.FileDTOS[0].Id
    }
    let hash = serialize(data).toString()
   
    getDownloadInfo(hash)
  }
  let showLink = article.BeforeId || article.AfterId;
  return (
    <div className="container">
      <Head>
        <title>{article.ContentDTO.Title}-文档下载-涨知识-优享社(youxiangshe.com)</title>
        <meta name="keywords" content={article.ContentDTO.Keywords}/>
        <meta name="description" content={article.ContentDTO.Description}/>
      </Head>
      <Nav/>
      <main>
        
        <div className={cx(st.nav,style.nav)}>
        当前位置：
        <a href="/">首页</a>>
        <a href="/k/list">涨知识</a>>
        <a href={`/k/list/${parent.Code}/1`}>{parent.Name}</a>>
        <strong>{article.ContentDTO && article.ContentDTO.Title}</strong>
        </div>

        <div className={st.content}>
          <div className={style.article}>
            <div className={style.body}>
            <h1>{article.ContentDTO && article.ContentDTO.Title}</h1>
            <p className={style.extra}>
              <span><img src="/time.png" alt="时间" />{article.ContentDTO && article.ContentDTO.CreatedOn}</span>
              <span><img src="/cc-eye.png" alt="阅览数" />{article.ContentDTO && article.ContentDTO.VisitTimes}</span>
            </p>
            <article className="article">
              <div className="machine" dangerouslySetInnerHTML={{__html:article.ContentDTO && article.ContentDTO.Content }}></div>
              <Iframe html={article.ContentDTO && article.ContentDTO.Content}/>
            </article>
            </div>
            {
              article.FileDTOS.length ? (
              <div className={style.download}>
              <form>
                <div>
                  <h3>填写资料，免费下载</h3>
                  <p>请认真填写资料，以便于我们为你推荐更多精华的内容</p>
                  <div className="formItem">
                    <label><em>*</em>姓名：</label>
                    <input value={form.ContactName} onInput={e=>updateForm(e,'ContactName')} />
                  </div>
                  <div className="formItem">
                    <label><em>*</em>手机：</label>
                    <input value={form.ContactMobile} onInput={e=>updateForm(e,'ContactMobile')} />
                  </div>
                  <div className="formItem">
                    <label><em>*</em>邮箱：</label>
                    <input value={form.ContactEmail} onInput={e=>updateForm(e,'ContactEmail')} />
                  </div>
                </div>
              </form>
              <div className={style.info}>
                <h5>
                  “如果喜欢此文章，点击下方按钮进行下载哦“
                </h5>
                <p>已经有{article.ContentDTO && article.ContentDTO.DownTimes} 人下载</p>
                <p><a className="btn" onClick={download}>立即下载（{article.FileDTOS.length && article.FileDTOS[0].Filesize}）</a></p>
                <p className={style.tips}>手机微信用户请使用右上角“在浏览器中打开“再下载</p>
              </div>
            </div> ):null
            }
                   
            {
              showLink ? (
              <div className={cx(style.links, 'clearfix')}>
                <div className={style.pre}>
                上一篇： 
                <a href={`/k/${type}/${article.BeforeId}`}>{article.BeforeTitle}</a>
                </div>

                <div className={style.after}>
                下一篇： 
                <a href={`/k/${type}/${article.AfterId}`}>{article.AfterTitle}</a>
                </div>
              </div>
              ) 
               : null
            }
            
          </div>
          <div className={style.hotList}>
            <section>
              <h2><img src="/icon.png" alt="相关阅读" />相关阅读</h2>
              <div>
                {
                  rec.map((item, index) => <a key={index} href={`/k/${type}/${item.ID}`}>{item.Title}</a>)
                }
              </div>
            </section>
            
            <section>
              <h2><img src="/hot.png" alt="热门电商文章排行" />热门电商文章排行</h2>
              <div>
                {
                  hot.map((item, index) => <a key={index} href={`/k/${type}/${item.ID}`}>{item.Title}</a>)
                }
              </div>
            </section>
          </div>
        </div>
      </main>
    <Footer/>
    </div>
  )
}

export async function getServerSideProps({ query }) {
  const { id, type } = query;
  const data = { typeCode: type, pageNo: 1, pageSize: 10, order: 'CreatedOn', isAsc: false, isCommend: 0}

  let res = await getTypes()
  const types = await res.json()
  let parent = types.find(item => item.Code == type)
  res = await getArticle({id})
  const article = await res.json()
  const hotReq = await getArticleList({...data, excludeId: id,order: 'VisitTimes' })
  const recReq = await getArticleList({...data, excludeId: id, isCommend: 1 })
  const contentReq = await getArticleList(data)
  const hot = await hotReq.json()
  const rec = await recReq.json()
  return {
    props: { 
    article, 
    type, 
    parent, 
    hot: hot.models,
    rec: rec.models,
    id }
  }
}

