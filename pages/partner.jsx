import Head from 'next/head'
import Footer from '@/components/footer'
import Nav from '@/components/nav'
import cx from 'classnames'
import st from '@/scss/provider.module.scss'
import style from '@/scss/knoweldge.module.scss'
import Iframe from '@/components/iframe'
import { getArticle } from '@/api/server'
export default function({ article}) {
  
  //let showLink = article.BeforeId || article.AfterId;
  return (
    <div className="container">
      <Head>
        <title>{article.ContentDTO.Title}-系统公告-优享社(youxiangshe.com)</title>
        <meta name="keywords" content={article.ContentDTO.Keywords}/>
        <meta name="description" content={article.ContentDTO.Description}/>
      </Head>
      <Nav/>
      <main>
        

        <div className={cx(st.content, st.singale)}>
          <div className={cx(style.article, style.post)}>
            <div className={style.body}>
            <h1>{article.ContentDTO && article.ContentDTO.Title}</h1>
            <article className="article">
              <div className="machine" dangerouslySetInnerHTML={{__html:article.ContentDTO && article.ContentDTO.Content }}></div>
              <Iframe html={article.ContentDTO && article.ContentDTO.Content}/>
            </article>
            </div>
            
          </div>
        
        </div>
      </main>
    <Footer/>
    </div>
  )
}

export async function getServerSideProps() {
  
  let res = await getArticle({id: '2020062016383200003'})
  const article = await res.json()
  return {
    props: { article }
  }
}