import Head from 'next/head'
import Nav from '@/components/nav'
import cogoToast from 'cogo-toast';
import { useState, useLayoutEffect } from 'react'
import { getFeedbackType, feedback } from '@/api/index'
import Footer from '@/components/footer'
import { serialize } from '@/utils/serialize'

export default function () {
  const [form, setForm] = useState({
    typeId: '',
    name: '',
    phone: '',
    email: '',
    content: ''
  })
  const [types, setTypes] = useState([])
  function updateForm(e, key) {
    e.preventDefault()
    let data = {...form}
    data[key] = e.target.value
    setForm(data)
  }
  function loginFn() {
    if (!form.typeId) return cogoToast.error('需要选择反馈类型');
    if (!form.name) return cogoToast.error('需要填写正确姓名');
    if (!form.phone ||
        !/^1[3-9]\d{9}$/.test(form.phone)
      ) return cogoToast.error('需要填写正确的联系电话');
    if (!form.email || 
        !/^[A-Za-z0-9]+([_\.][A-Za-z0-9]+)*@([A-Za-z0-9\-]+\.)+[A-Za-z]{2,6}$/.test(form.email)
      ) return cogoToast.error('需要填写正确的邮箱');
    if (!form.content) return cogoToast.error('需要填写正确的反馈内容');
    
    

    feedback(serialize(form)).then(data => {
      //todo: 跳转到后台管理端
      cogoToast.success('系统已经收到您的反馈')
    }).catch(err => {
      cogoToast.error('提交反馈失败了')
    })
  }
  function getTypes() {
    getFeedbackType().then(types => {
      setTypes(types)
      setForm({...form, typeId: types[0].Id})
    })
  }
  useLayoutEffect(() => {
    getTypes()
  }, [])
  return (
    <div className="container">
      <Head>
        <title>在线反馈-优享社(youxiangshe.com)</title>
        <meta name="keywords" content="在线反馈"/>
        <meta name="description" content="优享社在线反馈，可以将你的任何问题通过在线反馈的方式提交，优享社将会及时反馈。"/>
      </Head>
      <Nav/>
      <main className="default">
        <form>
          <h1>在线反馈</h1>
          <div>
            <div className="formItem">
              <label>反馈类型：</label>
              <select value={form.typeId} onChange={e => updateForm(e, 'typeId')}>
                {
                  types.map(item => <option key={item.Id} value={item.Id}>{item.Name}</option>)
                }
              </select>
            </div>
            <div className="formItem">
              <label>您的姓名：</label>
              <input value={form.name} onInput={e => updateForm(e, 'name')}></input>
            </div>
            <div className="formItem">
              <label>您的电话：</label>
              <input value={form.phone} onInput={e => updateForm(e, 'phone')}></input>
            </div>
            <div className="formItem">
              <label>您的邮件：</label>
              <input value={form.email} onInput={e => updateForm(e, 'email')}></input>
            </div>
            
            <div className="formItem">
              <label>反馈内容：</label>
              <textarea onInput={e => updateForm(e, 'content')} value={form.content}></textarea>
            </div>
            <div className="formItem">
              <label> </label>
              <a className="btn" onClick={loginFn}>提交反馈</a>
            </div>
          </div>
          <div>
         
        </div>
        </form>
      </main>
      <Footer/>
      
    </div>
  )
}
