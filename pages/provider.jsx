import Head from 'next/head'
import Footer from '@/components/footer'
import Nav from '@/components/nav'
import cx from 'classnames'
import style from '@/scss/provider.module.scss'
import { Link, animateScroll as scroll } from "react-scroll";
export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>供应商-优享社(youxiangshe.com)</title>
        <meta name="keywords" content="供应商入驻,品牌企业,商品分销"/>
        <meta name="description" content="供应商栏目包含供应商权益、入驻流程、资质要求、安全、保密等内容，帮助供应商了解入驻优享社的相关内容。"/>
      </Head>
      <Nav/>
      <div className={style.nav}>
          <h2>
            <Link 
              activeClass="active"
              to="card1"
              spy={true}
              smooth={true}
              offset={-140}
              duration={500}>
              供应商权益
              </Link>
          </h2>
          <h2>
            <Link 
              activeClass="active"
              to="card2"
              spy={true}
              smooth={true}
              offset={-140}
              duration={500}>
              资质要求
              </Link>
          </h2>
          <h2>
            <Link 
              activeClass="active"
              to="card3"
              spy={true}
              smooth={true}
              offset={-140}
              duration={500}>
              入驻流程
              </Link>
          </h2>
          <h2>
            <Link 
              activeClass="active"
              to="card4"
              spy={true}
              smooth={true}
              offset={-140}
              duration={500}>
              保密&安全
              </Link>
          </h2>
         
      </div>
      <main>
        <div className={style.content}>
          <div className={style.cardWrapper}>
            <div id ="card1" name="card1" className={cx(style.card, style.card1)}>
              <img src="/provider1.png" />
              <h2 className={style.title}>供应商权益</h2>
              <section>
                <p>优享社（youxiangshe.com），是一个为供应商提供线上全渠道销售的新型平台，供应商能够很容易的将商品（产品、服务）通过优享客，发散到全网络，甚至线下。</p>
                <h3>
                  供应商核心权益包括：
                </h3>
                <p>
                  <i>1、</i>将商品分发全网络，实现商品销售，扩大品牌传播
                </p>
                <p>
                  <i>2、</i>准入机制，有效降低优质供应商的竞争压力
                </p>
                <p>
                  <i>3、</i>获取客户、商机，构建企业私域流量
                </p>
                <p>
                  <i>4、</i>专家团队全程辅助，让你无忧、高效
                </p>
                <p>
                  <i>5、</i>CPS结算模式，包赚不赔
                </p>
                <div>

                </div>
              </section>
            </div>
            <div id="card2" name="card2" className={cx(style.card, style.card2)}>
              <img src="/provider2.png" />
              <h2 className={style.title}>资质要求</h2>
              <section>
                <h3>
                  <strong>供应商</strong>入驻需要具备如下资质：
                </h3>
                <p>
                  <i>1、</i>供应商必须具备合法有效的公司实体
                </p>
                <p>
                  <i>2、</i>上架商品、服务必须合法，且有相关审批文件
                </p>
                <p>
                  <i>3、</i>诚信经营，完善售后服务
                </p>
                <div>

                </div>
              </section>
            </div>
            <div id="card3" name="card3" className={cx(style.card, style.card3)}>
              <img src="/provider3.png" />
              <h2 className={style.title}>入驻流程</h2>
              <section>
                <div className={style.top}>
                  <div>step 1</div>
                  <div>step 2</div>
                  <div>step 3</div>
                  <div>step 4</div>
                </div>
                <div className={style.bottom}>
                  <div>
                    <p>第1步</p>
                    <h5>在线注册</h5>
                  </div>
                  <div>
                    <p>第2步</p>
                    <h5>签订协议</h5>
                  </div>
                  <div>
                    <p>第3步</p>
                    <h5>上架商品</h5>
                  </div>
                  <div>
                    <p>第4步</p>
                    <h5>入驻成功</h5>
                  </div>
                </div>
              </section>
            </div>
            <div id="card4" name="card4" className={cx(style.card, style.card4)}>
              <img src="/provider4.png" />
              <h2 className={style.title}>保密&安全</h2>
              <section>
                <h3>
                  为了保障双方的权益，优享社遵循以下规定：
                </h3>
                <p>
                  <i>1、</i>所有<strong>供应商</strong>提供的数据、资料等内容非授权不作他用
                </p>
                <p>
                  <i>2、</i>优享社对所有合作细节完全保密
                </p>
                <p>
                  <i>3、</i>优享社不进行任何有损供应商的事情
                </p>
                <p>
                  <i>4、</i>线上交易商品，优享社不获取交易用户的数据
                </p>
                <div>

                </div>
              </section>
            </div>
            <p className={style.register}>
              <a href="/register" className="btn">
                立即免费注册
              </a>
            </p>
          </div>
          <div className={style.articleList}>
            <section>
              <h2>入驻理由</h2>
            <div>
              <p>
                <span>1</span>优质小B全渠道带货
              </p>
              <p>
                <span>2</span>客户资料安全保密
              </p>
              <p>
                <span>3</span>全渠道统一商品素材
              </p>
              <p>
                <span>4</span>商机服务个性化支持
              </p>
              <p>
                <span>5</span>实名优享客，全流程追溯
              </p>
              <p>
                <span>6</span>定制化营销方案助力爆品
              </p>
              <p>
                <span>7</span>专家团队全程辅导
              </p>
            </div>
            </section>
            
            <p>
              <a href="/register" className="btn">
                立即入驻
              </a>
            </p>
          </div>
        </div>
      </main>
    <Footer/>
    </div>
  )
}



