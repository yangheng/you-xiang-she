import Head from 'next/head'
import Footer from '@/components/footer'
import Nav from '@/components/nav'
import cx from 'classnames'
import st from '@/scss/provider.module.scss'
import style from '@/scss/knoweldge.module.scss'
import Iframe from '@/components/iframe'
import { getArticle } from '@/api/server'
export default function({ article, id }) {
  
  let showLink = article.BeforeId || article.AfterId;
  return (
    <div className="container">
      <Head>
        <title>{article.ContentDTO.Title}-系统公告-优享社(youxiangshe.com)</title>
        <meta name="keywords" content={article.ContentDTO.Keywords}/>
        <meta name="description" content={article.ContentDTO.Description}/>
      </Head>
      <Nav/>
      <main>
        
        <div className={cx(st.nav,style.nav)}>
        当前位置：
        <a href="/">首页</a>>
        <a href="/posts">站内公告</a>>
        <a href={`/posts/${id}`}>{article.ContentDTO && article.ContentDTO.Title}</a>
        </div>

        <div className={st.content}>
          <div className={cx(style.article, style.post)}>
            <div className={style.body}>
            <h2>{article.ContentDTO && article.ContentDTO.Title}</h2>
            <article className="article">
              <div className="machine" dangerouslySetInnerHTML={{__html:article.ContentDTO && article.ContentDTO.Content }}></div>
              <Iframe html={article.ContentDTO && article.ContentDTO.Content}/>
            </article>
            </div>
            
            {
              showLink ? (
              <div className={cx(style.links, 'clearfix')}>
                <div className={style.pre}>
                上一篇： 
                <a href={`/posts/${article.BeforeId}`}>{article.BeforeTitle}</a>
                </div>

                <div className={style.after}>
                下一篇： 
                <a href={`/posts/${article.AfterId}`}>{article.AfterTitle}</a>
                </div>
              </div>
              ) 
               : null
            }
            
          </div>
        
        </div>
      </main>
    <Footer/>
    </div>
  )
}

export async function getServerSideProps({ query }) {
  const { id } = query;
  
  let res = await getArticle({id})
  const article = await res.json()
  return {
    props: { article, id }
  }
}