import Head from 'next/head'
import { useState, useEffect } from 'react'
import Footer from '@/components/footer'
import Nav from '@/components/nav'
import style from '@/scss/posts.module.scss'
import { getArticleList, getPostType } from '@/api/server'
import Pagination from 'rc-pagination';
export default function ({ list, page }) {
  function changePage(current) {
    window.location.href = `/posts/${current}`
  }
  return (
    <div className="container">
      <Head>
        <title>系统公告-优享社(youxiangshe.com)</title>
        <meta name="keywords" content="系统公告"/>
        <meta name="description" content="系统公告栏目是优享社发布的所有公告内容、重大变更等内容。"/>
      </Head>
      <Nav/>
      <main>

        <h1 className={style.nav}>系统公告</h1>  
        <div className={style.content}>
          <div className={style.body}>
            {
            list.map((item, index) => (
                <p key={index}>
                  <em>
                    {index}
                  </em>
                  <a href={`/posts/${item.ID}`}>
                    {item.Title}
                  </a>
                  <span>{item.CreatedOn}</span>
                </p>
              )
              )
            }
          </div>
          <div className="pagination">
            <Pagination current={page.current} total={page.total} pageSize={10} activeClassName="active" onChange={changePage}/>
          </div>
        </div>
      </main>
    <Footer/>
  </div>
  )
}

export async function getServerSideProps({ query }) {
  const data = { typecode: 'news', pageNo: 1, pageSize: 10, order: 'CreatedOn', isAsc: false, isCommend: 0}

  const { params } = query;
  let res = null;
  if (params) {
    data.pageNo = params[0];
  }
  res = await getArticleList(data)
  let list  = await res.json()
  return {
    props: { 
      list: list.models, 
      page: {
        total: list.total,
        current: data.pageNo
      }
    }
  }
}