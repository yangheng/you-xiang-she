import Head from 'next/head'
import Nav from '@/components/nav'
import Footer from '@/components/footer'
import style from '@/scss/home.module.scss'
import cs from 'classnames'
import { showCode } from '@/utils/serialize'
export default function Home() {
  
  return (
    <div className="container">
      <Head>
        <title>优享社(youxiangshe.com)，优秀的分享、分销社交电商平台</title>
        <meta name="description" content="优享社（youxiangshe.com）是一个优质商品发现、分享、分销的新型社交电商平台，供应商提供优质产品和服务，优享客分享商品赚取佣金。"/>
        <meta name="keywords" content="优享社,社交电商平台,分销,线上渠道"/>
      </Head>
      <Nav/>
      <main>
        
        <div className={style.banner}>
          <h1>
            优享社
          </h1>
          <div className={style.h2}>
            ——将优享生活带给身边每一个人
          </div>
          <p>
          
          <strong>优享社</strong>（youxiangshe.com）是一个优质商品<strong>发现、分享、分销</strong>的新型<strong>社交电商服务平台</strong>，<strong>优享社</strong>全球遴选优质产品，优享客分享赚取佣金
          
          
          </p>
          <div>
            <div className={style.share}>
              <span className={style.icon}>
                优
              </span>
              质优、价优、服务优
            </div>
            <div className={style.share}>
              <span className={style.icon}>
                享
              </span>
              分享商品、分享服务、分享体验
            </div>
            <div className={style.share}>
              <span className={style.icon}>
                社
              </span>
              经验交流、电商学院、全球互联
            </div>
          </div>
          {
            
            <a className={cs(style.register, 'btn')} onClick={showCode}>免费注册，成为优享客</a>
           
          }
          
        </div>
        <div className={style.benefit}>
          <h2 className={style.title}>
            <img alt="为你严选商品，无忧卖货" src="/icon-left.png"/>为你严选商品，无忧卖货<img alt="为你严选商品，无忧卖货" src="/icon.png"/>
          </h2>
          <div className={style.list}>
            <div>
              <img alt="无门槛" src="/home_image1.png"/>
              <div className="h2">无门槛</div>
              <p>⽆⻔槛成为优享客，不论居家还是副业，随时赚钱</p>
            </div>
            <div className={style.bottom}>
              <img alt="易上手" src="/home_image2.png"/>
              <div className="h2">易上手</div>
              <p>⼀键转卖，便捷操作易上⼿</p>
            </div>
            <div>
              <img alt="回报高" src="/home_image3.png" />
              <div className="h2">回报高</div>
              <p>佣⾦感⼈，20—50%业绩奖励 </p>
            </div>
            <div className={style.bottom}>
              <img alt="服务全" src="/home_image4.png" />
              <div className="h2">服务全</div>
              <p>专属服务，实时响应，提升卖货技能</p>
            </div>
          </div>
        </div>
        <div className={style.service}>
          <h2 className={style.title}>
            <img alt="360°贴心服务体系" src="/icon-left.png"/>360°贴心服务体系<img alt="360°贴心服务体系" src="/icon.png"/>
          </h2>
          <div className={style.list_wrapper}>
            <div className={style.left}>
              <div className={style.profile}>
                <img alt="个人主页" src="/home.png"/>
                <h3>个人主页</h3>
                <p>打造个性的个⼈⻚⾯，形成个⼈IP</p>
                <p>分享，展示你的⽣活品味</p>
                <p>线上线下全渠道覆盖，⼀键定制商品素材，⽅便把握每⼀次销售机会</p>
              </div>
              <div className={style.activity}>
                <img alt="促销活动" src="/cuxiao.png"/>
                <h3>促销活动</h3>
                <p>提供活动策划服务</p>
                <p>协助进⾏推⼴渠道的宣传⽂案策划</p>
              </div>
            </div>
            <div className={style.middle}>
              <img src="/360-all.png" alt="360°贴心服务体系"/>
            </div>
            <div className={style.right}>
              <div className={style.operation}>
                <img alt="运营指导" src="/yunying.png"/>
                <h3>运营指导</h3>
                <p>通过“涨知识”板块，了解带货王们的经营之道</p>
                <p>专家培训、同⾏交流，不断提升卖货技巧 </p>
                <p>电商知识学习，让你不断深⼊了解粉丝运营、销售技巧、选品技巧等必备知识</p>
              </div>
              <div className={style.number}>
                <img alt="数据透明" src="/touming.png"/>
                <h3>数据透明</h3>
                <p>佣金明细，了解每一笔收入来源</p>
                <p>业绩趋势、产品交易、客户分布等⼀⽬了然</p>
                <p>官方智能优化建议，加速你的成长</p>
              </div>
            </div>
          </div>

        </div>
        <div className={style.steps}>
          <h2 className={style.title}>
            <img alt="开启优享人生，仅需四步" src="/icon-white-left.png"/>开启优享人生，仅需四步<img src="/icon-white.png"/>
          </h2>
          <div>
            <div className={style.code}>
              <img src="/code.png" alt="优享社小程序二维码，扫码成为优享客"/>
              <div>扫码成为优享客</div>
            </div>
            <div className={style.step_wrapper}>
              <p className={style.top}>
                <span>successful</span>
              </p>
              <div className={style.bottom}>
                <div className={style.step1}>
                  <div className={style.h3}>Step1</div>
                  <p>第1步</p>
                  <p>访问微信小程序</p>
                </div>
                <div className={style.step2}>
                  <div className={style.h3}>Step2</div>
                  <p>第2步</p>
                  <p>实名注册</p>
                </div>
                <div className={style.step3}>
                  <div className={style.h3}>Step3</div>
                  <p>第3步</p>
                  <p>分享商品</p>
                </div>
                <div className={style.step4}>
                  <div className={style.h3}>Step4</div>
                  <p>第4步</p>
                  <p>赚取佣金</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={style.entry}>
          <h2 className={style.title}>
            <img alt="帮助供应商成功布局社交电商" src="/icon-left.png"/>帮助供应商成功布局社交电商<img src="/icon.png"/>
          </h2>
          <section>
            <div className={style.imgWrapper}>
              <img alt="帮助供应商成功布局社交电商平台" src='/02.png'/>
            </div>
            <div>
              <h3>便于操作：</h3>
              <p>系统化的产品管理流程</p>
              <h3>匹配资源：</h3>
              <p>高效为你匹配合适的小B资源</p>
              <h3>闪电入驻：</h3>
              <p>供应商准入，甄选机制，优质好货尽管拿来</p>
              <h3>打造包括：</h3>
              <p>联手打造爆款商品，高频展示，品效合一</p>
              <a href="/register"  className={cs(style.register, 'btn')}>供应商免费入驻</a>
              
            </div>
          </section>
        </div>
        <div className={style.social}>
          <h2 className={style.title}>
            <img alt="体验社交电商生态" src="/icon-left.png"/>体验社交电商生态<img src="/icon.png"/>
          </h2>
          <section>
            <div>
              <h3>快速入驻：</h3>
              <p>当天审核，模块式操作，帮你制作产品⻚⾯、全渠道推广素材</p>
              <p>积累客户，商机看得⻅</p>
              <h3>爆款打造：</h3>
              <p>根据分享数据，实时打造热点产品，好商品就有更多展示机会 </p>
              <p>产品定制建议，针对不同品类商品和⼈群，帮助策划更个性的产品形态</p>
              <p>更多样化的促销⽅式策划</p>
              <h3>数据分析：</h3>
              <p>根据商品分享情况，提供优化建议</p>
              <p>业绩趋势，客户反馈清晰可⻅</p>
              <h3>营销策划：</h3>
              <p>不会撰写产品推⼴⽂案？</p>
              <p>营销活动不知怎么弄？</p>
              <p>我们帮你搞定！</p>
              <a href="/register" className={cs(style.register, 'btn')}>供应商免费入驻</a>
              
            </div>
            <div className={style.imgWrapper}>
              <img alt="体验社交电商平台生态，全网分销" src='/03.png'/>
            </div>
          </section>
        </div>
      </main>
      <Footer/>
    </div>
  )
}
