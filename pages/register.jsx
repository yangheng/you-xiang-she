import Head from 'next/head'
import Nav from '@/components/nav'
import cogoToast from 'cogo-toast';
import cx from "classnames";
import { useState,useLayoutEffect } from 'react'
import { register,getCodeUri } from '@/api/index'
import { serialize } from '@/utils/serialize'
import Footer from '@/components/footer'
import style from '@/scss/login.module.scss'
const timeOpts = [
  '随时',
  '8~10',
  '10~12',
  '12~14',
  '14~16',
  '16~18',
  '18~20',
  '其他时间'
]
export default function Home() {
  const [form, setForm] = useState({
    companyName: '',
    linkman: '',
    mobile: '',
    linkTime: timeOpts[0],
    checkCode: ''
  })
  const [codeUri, setUri] = useState('')
  function updateUri() {
    getCodeUri().then(res => {
      setUri(res)
    })
    
  }
  function updateForm(e, key) {
    e.preventDefault()
    let data = {...form}
    data[key] = e.target.value
    setForm(data)
  }

  function loginFn() {
    if (!form.checkCode) return cogoToast.error('需要填写验证码'); 
    if (!form.companyName) return cogoToast.error('需要填写正确的公司名称');
    if (!form.linkman) return cogoToast.error('需要填写正确姓名');
    if (!form.linkTime) return cogoToast.error('需要选择联系时间');
    if (!form.mobile || !/^1[3-9]\d{9}$/.test(form.mobile)) return cogoToast.error('需要填写正确的联系电话');

    register(serialize(form)).then(data => {
      //todo: 跳转到后台管理端
      cogoToast.success('注册成功')
    })
  }
  useLayoutEffect(() => {
    updateUri()
    return () => {
      
    };
  }, [])

  return (
    <div className="container">
      <Head>
        <title>供应商注册-供应商-优享社(youxiangshe.com)</title>
        <meta name="keywords" content="供应商注册"/>
        <meta name="description" content="供应商注册入口，填写注册资料完成注册，成为优享社供应商。"/>
      </Head>
      <Nav/>
      <main className={cx(style.register, 'default')}>
        
        <form>
          <h1>供应商注册</h1>
          <div>
            <div className="formItem">
              <label>公司名称：</label>
              <input value={form.companyName} onInput={e=>updateForm(e, 'companyName')}></input>
            </div>
            <div className="formItem">
              <label>您的姓名：</label>
              <input value={form.linkman} onInput={e=>updateForm(e, 'linkman')}></input>
            </div>
            <div className="formItem">
              <label>联系电话：</label>
              <input value={form.mobile} onInput={e=>updateForm(e, 'mobile')}></input>
            </div>
            <div className="formItem">
              <label>联系时间：</label>
              <select value={form.linkTime} defaultValue={form.linkTime} onChange={e=>updateForm(e, 'linkTime')}>
                {
                  timeOpts.map((item,index) => <option key={index} value={item}>{item}</option>)
                }
              </select>
            </div>
            <div className="formItem">
              <label>验证码：</label>
              <input className="code" value={form.checkCode} onInput={e => updateForm(e,'checkCode')}></input>
              <img src={codeUri}/>
              <i onClick={updateUri}>刷新</i>
            </div>
            <div className="formItem">
              <label> </label>
              <a className="btn" onClick={loginFn}>立即注册</a>
            </div>
          </div>
          <div>
          
        </div>
        </form>
        <div className={style.notice}>
          <h4>注册须知：</h4>
          <p>1、注册留下联系信息，我们将有专人与您联系</p>
          <p>2、请填写真实信息，便于及时与您取得联系</p>
          <p>3、注册需要合同签订以后才能登录</p>
          <p>
            只支持供应商，优享客请访问微信小程序
          </p>
        </div>
      </main>
      <Footer/>
      
    </div>
  )
}
