import Document, { Html, Head, Main, NextScript } from 'next/document'

class Doc extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }
  render() {
    return (
      <Html>
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"></meta>
          <script dangerouslySetInnerHTML={{
            __html: `(function() {
              if (!0) return;
              var e = "abbr, article, aside, audio, canvas, datalist, details, dialog, eventsource, figure, footer, header, hgroup, mark, menu, meter, nav, output, progress, section, time, video".split(', ');
              var i= e.length;
              while (i--){
                  document.createElement(e[i])
              } 
          })() `
          }}>            
          </script>
        </Head>
        
        <body>
          <Main/>
          <NextScript/>
          <script dangerouslySetInnerHTML={{
            __html: `var _hmt = _hmt || [];
            (function() {
              var hm = document.createElement("script");
              hm.src = "https://hm.baidu.com/hm.js?b174b250afac46a7b363f79f08183cdc";
              var s = document.getElementsByTagName("script")[0]; 
              s.parentNode.insertBefore(hm, s);
            })();`
          }}></script>
          <script src="https://biz.youxiangshe.com/c/js/common/his.js" ownerId='2020060922592000001' source='PC' keywords='' type="text/javascript" charset="utf-8">
    </script>
        </body>
      </Html>
    )
  }
}

export default Doc
