import '@/scss/common.scss'
import 'normalize.css'
// import '@/utils/serialize'
function App({ Component, pageProps}) {
    return <Component {...pageProps} />
}
export default App