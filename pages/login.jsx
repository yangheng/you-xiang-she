import Head from 'next/head'
import Nav from '@/components/nav'
import cogoToast from 'cogo-toast';
import { useState, useLayoutEffect } from 'react'
import { getCodeUri, login } from '@/api/index'
import Footer from '@/components/footer'
import style from '@/scss/login.module.scss'
import { serialize } from '@/utils/serialize'
export default function Home() {
  const [form, setForm] = useState({
    loginName: '',
    password: '',
    code: ''
  })
  const [codeUri, setUri] = useState('')
  
  function updateUri() {
    getCodeUri().then(res => {
      setUri(res)
    })
    
  }
  function updateForm(e, key) {
    e.preventDefault()
    let data = {...form}
    data[key] = e.target.value
    setForm(data)
  }
  function loginFn() {
    if (!form.loginName) return cogoToast.error('需要填写正确的用户名');
    if (!form.password) return cogoToast.error('需要填写密码');
    if (!form.code) return cogoToast.error('需要填写验证码');
    
    login(serialize(form)).then(data => {
      //todo: 跳转到后台管理端
      cogoToast.success('成功登录')
      window.location.href = 'https://biz.youxiangshe.com/provider/default.aspx'
    }, () => {
      updateUri()
    })
  }
  useLayoutEffect(() => {
      updateUri()
      return () => {
        
      };
    }, [])
  return (
    <div className="container">
      <Head>
        <title>系供应商登录-供应商-优享社(youxiangshe.com)</title>
        <meta name="keywords" content="供应商登录"/>
        <meta name="description" content="供应商登录入口，填写登录信息登录，进入供应商后台。"/>
      </Head>
      <Nav/>
      <main className="default">
        <form>
          <h1>供应商登录</h1>
          <div>
            <div className="formItem">
              <label>用户名：</label>
              <input value={form.loginName} onInput={e => updateForm(e,'loginName')}></input>
            </div>
            <div className="formItem">
              <label>密码：</label>
              <input type="password" value={form.password} onInput={e => updateForm(e,'password')}></input>
            </div>
            <div className="formItem">
              <label>验证码：</label>
              <input className="code" value={form.code} onInput={e => updateForm(e,'code')}></input>
              <img src={codeUri}/>
              <i onClick={updateUri}>刷新</i>
            </div>
            <div className="formItem">
              <label> </label>
              <a className="btn" onClick={loginFn}>登录</a>
            </div>
          </div>
          <div>
          <p className={style.formTips}>
            <a href="/forget">
              忘记密码
            </a>
            <a href="/register">
              供应商注册
            </a>
            <p>
            只支持供应商，优享客请访问微信小程序
          </p>
          </p>
          
        </div>
        </form>
        
      </main>
      <Footer/>
      
    </div>
  )
}
